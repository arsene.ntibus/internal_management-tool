<?php ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);?><!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion de Project | Simplon.co </title>
    <link rel="stylesheet" href="style/style.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>

<?php
// CRUD des Projet

require '../controller/crud/projet/create.php';
require '../controller/crud/projet/read.php';
require '../controller/crud/projet/delete.php';
require '../controller/crud/projet/update.php';
// CRUD des  developer

require '../controller/crud/developer/read.php';
require '../controller/crud/developer/delete.php';
require '../controller/crud/developer/update.php';
//  Header du HTML
include '../header.php';

// router
//  analye les paramaetre dans la request et  redirige en f(X) de la  dans la  requeste  redige ves des fontion ici
//  exmeple readProject() ,
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'read') {
        echo readProject($_GET['id']);
    } elseif ($_GET['action'] == 'updateprojectform') {
        updateProjectForm($_GET['id']);
    } elseif ($_GET['action'] == 'update') {
        echo updateProject($_GET['id']);
    } elseif ($_GET['action'] == 'createprojectform') {
         createProjectForm();
    } elseif ($_GET['action'] == 'create') {
        echo createProject();
    } elseif ($_GET['action'] == 'delete') {
        deleteProject($_GET['id']);
    } else {
        include '../404NotFound.html';
    }
} else {
    include '../content.php';
}
// footer du HTML
include '../footer.php';
