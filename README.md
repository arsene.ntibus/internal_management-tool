<h1 align="center">Bienvenue dans internal_management-tool 👋</h1>



Table of Contents
-----------------

1. [Introduction](#introduction)
2. [PREREQUISITE](#Prerequis)
3. [FileSystem](#Filesystem)
4. [MVC](#mvc)
* [CRUD](#CRUD)
  * [Comment il fontionne](#USAGE)
      * [Create](#Ajouter)
      * [Read](#Afficher)
      * [update](#MiseAJour)
      * [Delete](#DELETE)
  
* [License](#license)

#Introduction

Bonjour, bienvenue dans ce magnifique projet, je sais qu'il n'est pas entièrement terminé, mais je vous assure 
que les principales caractéristiques sont opérationnelles.
Dans cette application, vous pouvez ajouter un projet, modifier un projet, voir les projets et les supprimer.

<br>
<br>
Dans une version future de cette application vous pourrez ajouter des développeurs et des Clients .
Vous pourrez aussi les assigner à un ou plusieurs projets que le clients aura.
Vous pourrez aussi Administrer les projets, les clients, ansi que les projets. 


#Prerequis

Pour faire usage de cette application, vous devez avoir installé LAMP sur votre O.S. pour pouvoir lancer un serveur sur le port de votre choix ;
il est préférable de lancer le serveur sur le port 8000.
<br>
to Pour lancer le server, tapper la commande ci-dessous dans le dossier public 

```
php -S localhost:8000
```
<p>

# FileSystem

## MVC
J'ai opté pour un parton de conception en MVC 
 à savoir un dossier Model , un Dossier View un dossier controller
Pour le moment, seulmenent le dosseier projet dans controller fonctionne, les dossiers view et model seront
implémentés dans les versions futures de l'application. 


Pour le moment, seul le dossier du projet dans le dossier du contrôleur fonctionne, il est utilisé pour 
être en mesure de modifier, ajouter et supprimer des projets dans la base de données.
projets dans la base de données, le dossier développeur fonctionnera, espérons-le, 
dans une version future de l'application.

##CRUD

Cette Application en l'étât permet d'ajouter, de modifier des projets et de les supprimer 
mais aussi deles afficher.
<br>
##Ajouter
 Pour ajouter un projet,cliquer su le boutont bleu Ajouter , cela vous redirigira vers la page d'ajout de projet, 
 rensignez y le nom du projet anisi qu'un descriptif de celui ci.
#Afficher
<br>
Pour Afficher le projet que vous venez d'ajouter, Appiuez sur le lien voir. 
vous verrez le titre ansi que la description.
Vous pouvez appuyer sue le lien retour pour revenir à l'accueil.
<br>

#MiseàJour
Pour Modifier le projet, appuyer sur le lien modifier, 
vous pourrez modifier le titre ainsi que le descripitif de celui-ci. 
<br>
#Delete
Si vous désirez supprimez un projet, cliquez sur le lien supprimer 



#Liscence 
Give a ⭐️ if this readme helped you helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_